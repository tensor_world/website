---
layout: page
sidebar_link: true
permalink: /toolboxes/
title: Toolbox and links
---

This table is a tentative to collect all existing tensor decomposition
toolboxes. Please contribute if you notice any toolbox missing or any outdated
data.

Also, note that there exist a [Wikipedia Page](https://en.wikipedia.org/wiki/Tensor_software) that also collects many references.

Finally, one should also mention [PyTorch](https://pytorch.org/) and [TensorFlow](https://www.tensorflow.org/). These softwares handle
tensor manipulation very efficiently, since designed for extremely large data
sets and GPU-based computing. They do not implement however tensor operators,
as far as I know.

|Name |Institution |Language  |Key words  |Link| Maintained |
|-|-|
| 3WayPack | Leiden University | Fortran | psychometrics, interface | [3mode compagny](http://three-mode.leidenuniv.nl/) | yes |
|-|-|
| HOTTBOX | Imperial College London | Python | High order tensors, Classification | [@github](https://github.com/hottbox/hottbox)| yes |
|-|-|
| multiway package | University of Minnesota | R | psychometrics, indscale | [N. Helwig's page](http://users.stat.umn.edu/~helwig/software.html) | ? |
|-|-|
| N-way | University of Copenhaguen | Matlab | chemometrics, ALS, PARAFAC2 | [@Mathworks](https://uk.mathworks.com/matlabcentral/fileexchange/1088-the-n-way-toolbox) | yes |
|-|-|
| SPLATT | University of Minnesota | C |  sparsity, parallel computing, Big Data | [@Github](https://github.com/ShadenSmith/splatt/)| yes? | 
|-|-|
| Tensorbox | Riken | Matlab | neuroscience | [A. Phan's page](http://www.bsp.brain.riken.jp/~phan/#tensorbox)| ? |
|-|-|
| Tensorlab (v3)| KU Leuven| Matlab | structure, second order methods | [Tensorlab](https://www.tensorlab.net/)| yes |
|-|-|
| TensorLy | ? | Python | tensor operations, neural networks | [@Github](http://tensorly.org/stable/index.html) | yes |
|-|-|
| Tensor Toolbox | Sandia National Laboratories | Matlab | storage, sparsity, first order methods | [@Gitlab](https://www.tensortoolbox.org) | yes |
|-|-|
| Tensor Toolbox (jl) | EPFL | Julia | tensor formats, clone of Tensor Toolbox with TT support | [@Github](https://github.com/lanaperisa/TensorToolbox.jl) | yes |
|-|-|
| TTpy | Skoltech | Python | tensor train | [@Github](https://github.com/oseledets/ttpy) | yes? |
|-|-|
| TT-toolbox | Skoltech | Matlab | tensor train | [@Github](https://github.com/oseledets/TT-Toolbox) | yes? |
|-|-|

Also, tensor codes can be found at the following pages:
- About scientific computing (more references needed here!!):
    - [J. J. Li's page](http://fruitfly1026.github.io/). Check out for instance [InTensLi](https://github.com/hpcgarage/InTensLi) for fast MTTKRP.
    - The [Planc](https://ramkikannan.github.io/planc-api/index.html) and [TuckerMPI](https://gitlab.com/tensors/TuckerMPI) projects for respectively computationally fast nonnegative matrix/tensor factorization and computationally fast Tucker decomposition, based on efficient implementations and parralelisations or tensor operators.

- [Tensor package](http://www.gipsa-lab.grenoble-inp.fr/~pierre.comon/TensorPackage/tensorPackage.html)
- [G. Zhou's page](http://bsp.brain.riken.jp/~zhougx/tensor.html)
- [P. Tichavsky's page](http://si.utia.cas.cz/downloadPT.htm)
- [K. Huang's page](https://www.cise.ufl.edu/~kejun/index.html) with AOADMM and variants, under construction at the moment.
- [J.E.Cohen's repository](https://github.com/cohenjer/Tensor_codes) (some self
  advertising)
- many more...

<!--
| AOADMM | University of Minnesota | Matlab |constrained decomposition, proximal methods | [K. Huang's page](https://sites.google.com/a/umn.edu/huang663/)| ? |
-->

