---
layout: page
title: The project
sidebar_link: true 
---
## Why a tensorworld project

- As of today, tensorworld is mainly a webpage collecting various toolboxes for tensor decompositions and related problems, and advertising interesting new toolboxes projects. 
- The actual goal of tensorworld is to go further: collect data sets, host news about conferences or important meetings, reference important bibliography...
- Maybe this can also become a hub for the community to exchange ideas.

In the end, tensorworld is meant to be a community project. Anyone interested
in joining us to make this project live on is welcome to contact us at 
``` 
tensor_world@googlegroups.com
```

Currently, it is maintained by Jeremy Cohen (IRISA, Rennes, France) and hosted
thanks to Tammy Kolda purchasing the domain name.

### Todo list:

(Any help is welcome!)

- Add a list of interesting documented toy data sets for playing around or
  using in articles. Check for instance the [FROSTT](http://frostt.io/) repository for large sparse data, or [Rasmus Bro's webpage](http://www.models.life.ku.dk/datasets).
- Add a news section about tensor-related conferences, special issues in
  journals and other events of interest.
- Add a list of useful references (books, book chapters, important historical
  articles). 

 Note that some of this content can already be found at the excellent webpage of Pieter M. Kroonenberg, the [Three-Mode Company](http://three-mode.leidenuniv.nl/).

<!--
# Tensor World webpage
(powered by the [Hydeout](https://fongandrew.github.io/hydeout/) theme for Jekyll)

This website is meant as a portal for the tensor decomposition community to
share information and collaborate on the open project Tensor_World. This
website function is, on top of project related communication, to facilitate
open and reproducible research, as well as referencing. 

Indeed, this repository makes it easy and accessible for anyone to write a blog
post on the website, add content to an existing page (such as the list of
existing toolboxes), or to even add a page devoted to a specific topic.

### Joining the project

Before anything else, if you are interested in the project and want to
contribute, you will likely need to join the gitlab tensor_world group. for
this,
   - Create a Gitlab account (you can loggin with a Github account).
   - Send an email to tensor.world@gmail.com and ask for rights in the project.
     You will then be able to add and modify files in all projects inside the
     tensor_world group.

### Site organization

Pages are contained in the pages directory, and blog posts are contained in
the \_posts directory. You should not need to open other directories for adding
content (unless you want to modify the webpage appearnace, which I do no
recommand).

### Usage

As a researcher on tensor decomposition methods, you are likely to detain
helpful information to all the community on
- how to developp an open source
tensor toolbox for such a large community
- how we should procede in practice (organization of seminars - hackathons -
  special sessions - workshops, program structure etc)
- foundings and general organization

To share all these informations easily, you are very welcome to write a post on
the website. You can keep in track with what is posted online by subscribing to
the [RSS flux]({{site.baseurl}}/{{site.feed.path | default: 'feed.xml' }} ).

#### How to write a post, step by step

I will now explain how to post (it is very simple) directly on Gitlab, but you
can of course clone the repository on your local machine and push your post written offline.

###### Step 1: Create a markdown file

Open the \_posts folder. Using the + icon, create a new file. The name <b> HAS
TO START </b> with a date, preferably the date you are writing the post. For
instance, the 18st January 2054, you would write:

```
2054-01-18-anything-works.md
```

You may add any name after the date in the new file name, but do not forget the .md
extension.

###### Step 2: Adding a header to your .md file

Second, you need to write a very simple header, so that Jekyll (the web site
generator) can process it.

This goes as follows:
```
---
layout: post
title: your-post-title
category:
  - Discussions
author: your-name
excerpt_separator:  
---
```
Simply copy-paste these lines of code, replacing the title and the name. 

###### Step 3: Write your post in Markdown (basically plain text), and/or/also HTML

You are all set to starting writing. The .md format allows to type using
text-like syntax, more information is provided [here](https://daringfireball.net/projects/markdown/syntax). I suggest to use other
posts as models for the syntax, but basically just write it like you would in a
plain text editor.

If you already know how to write in HTML, you can also use it to add links or
small pictures.

Finally, if you post is long, use the <! - -more- - > tag to cut the post in two: a
part shown in the blog summary (preferably only a paragraph), and a second long
part shown when clicking on the post on the website.


### Heavy modding

Additionally, you may contribute to the structure of the website itself by
playing around with the features of the Hydeout theme for Jekyll. For
information on how to do that, please refer to the [Hydeout github
repository](https://github.com/fongandrew/hydeout).

-->

<!--
## Collaboration tools

To start discussions, we have set a few tools:
  - A [Gitlab group](https://gitlab.com/tensor_world) where we can create projects to work on code (this website, toolbox
        code, documents, maybe bibtex files?, etc). Everything will be open source and collaborative, and anyone
        interested in the project will be given access. How to join the project
        is detailled in the [HowTo]( {{ site.baseurl }}/README.html ) page.
  - A Google mailing list : 
   
  ```
  tensor_world@googlegroups.com
  ```
   
  Any interested person may join this mailing list, to be informed of the project
  advancement, by sending a request email at 
  ```
  tensor_world+subscribe@googlegroups.com
  ```


## Motivations

### Few quick words
In TRICAP2018 in New-Mexico, Pieter Kroonemberg gave an inspiring talk about
one of the work of his life: the 3WayPack. It is a user-friendly software that
implements some workhorse tensor decomposition methods, along with data
visualization and manipulation. 

We are all aware of the multitude of toolboxes available nowadays. Basically
every research team working on tensors has its own. At the end of his talk,
Pieter suggested that, for our research and implementation efforts to survive
aging, we should work as a community and build a common software that we could
all agree on, and which would help newcomers to get familiar with tensor
methods. 

As far as I am concerned, I think we need even more that a collaborative C or python
package. Right now, this community only has TRICAP as a mean to gather and
exchange extensively. In an age where collaboration has been made so easy, we
should be able to communicate way more. This is why I am launching this
Tensor\_World Project with the help of other researchers from both the applied
and computer worlds: Tamara Kolda, Shaden Smith and Rasmus Bro. I expect many
more to join this journey, and expect some of you to be skeptical as well.

Many of whom I could speak to agreed that something should be done to improve
both collaborations among us and visibility of tensor methods. But we don't all
mean the same thing. The first step of this project will be to start
discussions, and specify the need for several tools that we can all agree we
need. I expect this project to split in several sub-projects
(website, wiki, toolbox, conferences...). I would love to meet all the
interested researchers, in about a year from now, in a symposium or hackathon,
to summarize the discussion we will have had up to that point, and start making
actual decisions about practical issues such as time, money and involvement.

Jeremy Cohen


## Challenges

Here are some of the challenges that we may want to tackle as a community:
  - Tensor-related research papers have increased significantly in number
        lately. It is becoming challenging to keep up with the most recent
        applications fields and exotic techniques. 
  - We all have some a-priori information about which algorithm/model we
        like the most. But we often do not have easy and fair means of
        comparison among them.
  - To reduce the noise generated by the existence of multitudes of
        toolboxes. If possible, having a package that we all acknowledge,
        containing state-of-the-art, efficiently implemented algorithms for
        computing workhorse decomposition models such as PARAFAC and its
        variants. A minimum contribution would be to guide newcomers in the
        existing forest of toolboxes by providing a comprehensive list and
        guide.
  - We need to think of what will become of our present implementations
        in the next 20 or 30 years. I fear that, I we do not gather and produce
        a unified framework for our (and others) future contributions to latch onto, most of
        our present work will go to waste.

-->
